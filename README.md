# Amy's unofficial Vale rules

These rules are not endorsed by my employer. This repository is so I can track
rules I write and find useful, even if the Technical Writing team at GitLab
does not adopt them.

[[_TOC_]]

## What's Vale?

An open-source command-line tool for linting. See [vale.sh](https://vale.sh/)
for useful explanations and instructions.

## Contributing

I doubt anyone but me will ever find this repo, much less use its contents.
If you're here, though, and you find any of these rules useful, great!
Should you find bugs / problems / improvements, let me know.

## Project status

This isn't a project I "develop." I'm just capturing useful things as I need them.
You shouldn't expect any kind of releases, _per se_.
